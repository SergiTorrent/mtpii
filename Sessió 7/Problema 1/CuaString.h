#ifndef CUASTRING_H
#define CUASTRING_H
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;


class CuaString
{
// Descripci�: una cua d�strings
public:
// CONSTRUCTORS I DESTRUCTOR ----------------------------------
    CuaString();
// Pre: --; Post: cua buida
    CuaString(const CuaString& o); // const. de c�pia
// Pre: --; Post: aquesta cua �s c�pia de la Cua o
    ~CuaString();
// Pre: --; Post: mem�ria alliberada
// CONSULTORS -------------------------------------------------
    bool buida() const;
// Pre: -- ; Post: retorna cert si la cua �s buida; fals en c.c.
    void primer() const;
// Pre: cua no buida; Post: retorna c�pia del primer Pacient de la cua
// MODIFICADORS -----------------------------------------------
    void encua(int c,string n,string cog,int h);
// Pre: --; Post: ha afegit s al final de la cua
    void desencua();
// Pre: cua no buida; Post: ha eliminat el primer element de la cua
// OPERADORS REDEFINITS --------------------------------------
    CuaString& operator=(const CuaString& o);
// Pre: -- ; Post: aquesta cua �s c�pia de o
    int diferenciahora(int hactual);

private:
    struct Node
    {
        int codi;
        string nom;
        string cognom;
        int hora;
        Node* seg;
    };
// ATRIBUTS
    Node* a_primer; // punter al primer de la cua
    Node* a_darrer; // punter al darrer de la cua
// M�TODES PRIVATS
    void copia(const CuaString& o)
    {
// Pre: cua buida; Post: aquesta cua �s c�pia de o
// COST: O(n)
        Node* p = o.a_primer;
        while(p!=NULL)  // recorrem cua o
        {
            encua(p->codi,p->nom,p->cognom,p->hora); // this->encua(p->valor)
            p=p->seg;
        }
    }
    void allibera()
    {
// Pre: --; Post: cua buida
// COST: O(n)
        while (a_primer!=NULL)  //!buida()
        {
            Node* aux= a_primer;
            a_primer= a_primer->seg;
            delete aux;
        }
        a_darrer= NULL;
    }

};

#endif // CUASTRING_H
