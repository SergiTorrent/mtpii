#include "CuaString.h"



CuaString::CuaString()
{
// Pre: --; Post: cua buida
// COST: O(1)
    a_primer=a_darrer= NULL;
}
CuaString::CuaString(const CuaString& o)
{
// Pre: --; Post: aquesta cua �s c�pia de la cua o
// COST: O(n)
    a_primer=a_darrer= NULL;
    copia(o); // crida a m�tode privat
}
CuaString::~CuaString()
{
// Pre: --; Post: mem�ria alliberada
// COST: O(n)
    allibera(); // crida a m�tode privat
}


// CONSULTORS
bool CuaString::buida() const
{
// Pre: -- ; Post: retorna cert si la cua �s buida; fals en c.c.
// COST: O(1)
    return a_primer==NULL;
}
void CuaString::primer() const
{
// Pre: cua no buida; Post: retorna c�pia del primer de la cua
// COST: O(1)
    cout<<"CODI: "<<a_primer->codi<<endl;
    cout<<"NOM: "<<a_primer->nom<<" ";
    cout<<a_primer->cognom<<endl;
    int hora, minuts;
    minuts=a_primer->hora%100;
    hora=a_primer->hora/100;
    if (hora!=0){
        cout<<"HORA ENTRADA: "<<setfill('0')<<setw(2)<<hora<<":";
    }
    else cout<<"HORA ENTRADA: 00:";
    if (minuts!=0){
        cout<<setfill('0')<<setw(2);
        cout<<minuts<<endl;
    }
    else cout<<"00"<<endl;
}
// MODIFICADORS
void CuaString::encua(int c,string n,string cog,int h)
{
// Pre: --; Post: ha afegit s al final de la cua
// COST: O(1)
    Node* nou= new Node;
    nou->codi= c;
    nou->nom= n;
    nou->cognom= cog;
    nou->hora= h;
    nou->seg= NULL;
    if (buida()) // this->buida()
        a_primer=a_darrer= nou;
    else
    {
        a_darrer->seg= nou;
        a_darrer= nou;
    }
}
void CuaString::desencua()
{
// Pre: cua no buida; Post: ha eliminat el primer de la cua
// COST: O(1)
    Node* aux= a_primer;
    if (a_primer==a_darrer) // nom�s hi ha un element
        a_primer=a_darrer= NULL;
    else
        a_primer= a_primer->seg;
    delete aux;
}
// OPERADOR ASSIGNACI�
CuaString& CuaString::operator=(const CuaString& o)
{
// Pre: -- ; Post: aquesta cua �s c�pia de o
// COST: O(n)
    if (this != &o)
    {
        allibera();
        copia(o);
    }
    return *this;
}

int CuaString::diferenciahora(int hactual)
{
        int amin,ahora,actual_hora,actual_minuts;
        int minuts,hora;
        ahora=hactual/100;
        amin=hactual%100;
        hora=a_primer->hora/100;
        minuts=a_primer->hora%100;
        actual_minuts=amin-minuts;
        actual_hora=ahora-hora;
        if (actual_hora<0)
        {
            actual_hora=(ahora+24)-hora;
        }
        return actual_minuts+actual_hora*60;
}

