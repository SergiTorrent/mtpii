#include <iostream>
#include <string>
#include "CuaString.h"

using namespace std;

void menu()
{
    cout<<"OPCIONS:"<<endl;
    cout<<"1: ENTRAR UN PACIENT"<<endl;
    cout<<"2: SORTIR UN PACIENT"<<endl;
    cout<<"3: MOSTRAR LLISTA D'ESPERA"<<endl;
    cout<<"4: MOSTRAR NOMBRE DE PACIENTS EN ESPERA"<<endl;
    cout<<"5: MOSTRAR RESUM DELS PACIENTS ATESOS"<<endl;
    cout<<"9: MOSTRAR EL MENU D'OPCIONS"<<endl;
    cout<<"0: SORTIR DEL PROGRAMA"<<endl;
}

int main()
{
    menu();
    int pactuals=0;
    int patesos=0;
    double ttotal=0;
    CuaString Pacients;
    int opcio;
    cout<<"OPCIO:"<<endl;
    cin>>opcio;
    while (opcio!=0)
    {
        if (opcio==1)
        {
            int codi,hora;
            string nom,cognom;
            cout<<"ENTRA EL PACIENT:"<<endl;
            cout<<"CODI:"<<endl;
            cin>>codi;
            cout<<"NOM:"<<endl;
            cin>>nom;
            cin>>cognom;
            cout<<"HORA ENTRADA:"<<endl;
            cin>>hora;
            Pacients.encua(codi,nom,cognom,hora);
            pactuals++;
        }
        else if (opcio==2)
        {
            if (Pacients.buida()) cout<<"NO HI HA PACIENTS ESPERANT"<<endl;
            else
            {
                cout<<"HORA ACTUAL:"<<endl;
                int horaactual;
                cin>>horaactual;
                cout<<"SURT PACIENT:"<<endl;
                Pacients.primer();
                cout<<"TEMPS D'ESPERA: ";
                cout<<Pacients.diferenciahora(horaactual)<<" MINUTS"<<endl;
                ttotal=ttotal+Pacients.diferenciahora(horaactual);
                Pacients.desencua();
                pactuals--;
                patesos++;
            }
        }
         else if (opcio==3)
        {
                CuaString PSortida;
                PSortida=Pacients;
                cout<<"LLISTAT DELS PACIENTS"<<endl;
                cout<<"-------------------------"<<endl;
                while (!PSortida.buida())
                {
                    PSortida.primer();
                    PSortida.desencua();
                    cout<<"-------------------------"<<endl;
                }

        }
         else if (opcio==4)
        {
                cout<<"NOMBRE DE PACIENTS EN ESPERA:"<<endl<<pactuals<<endl;
        }
         else if (opcio==5)
        {
                cout.setf(ios::fixed);
                cout.precision (1);
                cout<<"NOMBRE DE PACIENTS ATESOS:"<<endl<<patesos<<endl;
                if (patesos!=0){
                cout<<"TEMPS MIG D'ESPERA:"<<endl<<ttotal/patesos<<endl;
                }
        }
         else if (opcio==9)
        {
            menu();
        }
        cout<<"OPCIO:"<<endl;
        cin>>opcio;
    }
    cout<<"FINAL DEL PROGRAMA"<<endl;
    return 0;
}
