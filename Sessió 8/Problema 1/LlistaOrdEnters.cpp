#include "LlistaOrdEnters.h"
LlistaOrdEnters::LlistaOrdEnters()
{
// Pre:--; Post: llista buida
    a_inici = NULL;
}
LlistaOrdEnters::LlistaOrdEnters(const LlistaOrdEnters &o)
{
// Pre:--; Post: la llista �s c�pia de o
    a_inici = NULL;
    copiar(o);
}

LlistaOrdEnters::~LlistaOrdEnters()
    {
// Pre: --; Post: objecte destru�t
        alliberar();
    }
bool LlistaOrdEnters::existeix(int e) const
{
// Pre:--; Post: ens diu si e existeix a la llista
    Node *p = a_inici;
    bool fiCerca = false;
    bool trobat = false;
    while ((p!=NULL) && (!fiCerca))
    {
        if (p->dada >= e)
        {
            fiCerca= true;
            trobat = p->dada == e;
        }
        else
            p = p->seg;
    }
    return trobat;
}
bool LlistaOrdEnters::buida() const
{
// Pre:--; Post: ens diu si la llista �s buida
    return a_inici == NULL;
}

LlistaOrdEnters& LlistaOrdEnters::operator=(const LlistaOrdEnters& o)
{
// Pre: -- ; Post: aquesta llista �s c�pia de o
// COST: O(n)
    if (this != &o)
    {
        alliberar();
        copiar(o);
    }
    return *this;
}




void LlistaOrdEnters::llistar() const
{
// Pre:
// Post:
    Node *p= a_inici;
    while ( p!=NULL )
    {
        cout<<p->dada<<" ";
        p=p->seg;
    }
}



void LlistaOrdEnters::inserir(int e)
{
// Pre:
// Post:
    Node *p, *ant, *nou;
    bool fiCerca= false;
    bool trobat= false;
    ant = NULL;
    p = a_inici;
    while((p!=NULL) && (!fiCerca))
    {
        if (p->dada >= e)
        {
            fiCerca=true;
            trobat=p->dada==e;
        }
        else
        {
            ant=p;
            p=p->seg;
        }
    }
    if (!trobat)
    {
        nou=new Node;
        nou->dada=e;
        nou->seg=p;
        if(ant==NULL)  // casos l�mit
        {
            a_inici=nou;
        }
        else
        {
            ant->seg=nou;
            nou->seg=p;
        }
    }
}



void LlistaOrdEnters::esborrar(int e)
{
// Pre:
// Post:
    Node *p, *ant;
    bool fiCerca = false;
    bool trobat = false;
    p = a_inici;
    ant = NULL;
    while((p!=NULL) && (!fiCerca))
    {
        if (p->dada >= e)
        {
            fiCerca=true;
            trobat=p->dada==e;
        }
        else
        {
            ant=p;
            p=p->seg;
        }
    }
    if (trobat)
    {
        if (ant==NULL)
        {
            a_inici=p->seg;
        }
        else
        {
            ant->seg=p->seg;
        }
    }
    delete p;
}






