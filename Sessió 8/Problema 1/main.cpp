#include <iostream>
#include "LlistaOrdEnters.h"

using namespace std;

int main()
{
    cout << "Entra la llista d'enters acabada amb 0:" << endl;
    int i,e;
    LlistaOrdEnters enters;
    cin>>i;
    while (i!=0)
    {
        enters.inserir(i);
        cin>>i;

    }
    cout << "Entra un enter per a ser esborrat:" << endl;
    cin>>e;
    cout << "Llista ordenada sense repetits:" << endl;
    enters.llistar();
    cout<<endl;

    if (!enters.existeix(e))
    {
        cout << "L'element "<<e<<" no hi es a la llista" << endl;
    }
    else {
        cout << "L'element "<<e<<" existeix a la llista" << endl;
    }
        enters.esborrar(e);
        cout<<"Llista ordenada sense el possible element a esborrar: "<<endl;
        enters.llistar();
        cout<<endl;
    return 0;
}
