#ifndef LLISTAORDENTERS_H
#define LLISTAORDENTERS_H
#include <iostream>

using namespace std;


class LlistaOrdEnters
{
//cal afegir algunes pres i posts
//cal acabar d'implementar alguns m�todes
public:
    LlistaOrdEnters();
    LlistaOrdEnters(const LlistaOrdEnters &o);
    ~LlistaOrdEnters();
    LlistaOrdEnters &operator=(const LlistaOrdEnters &o);
    bool existeix(int e) const;
    bool buida() const;
    void llistar() const;
    void inserir(int e);
    void esborrar(int e);
private:
    struct Node
    {
        int dada;
        Node *seg;
    };
    Node *a_inici;

    void copiar(const LlistaOrdEnters& o)
    {
// Pre: llista buida; Post: aquesta llista �s c�pia de o
        Node* p = o.a_inici;
        Node* ant = NULL;
        while (p!=NULL)  // recorrem
        {
            Node* nou= new Node;
            nou->dada = p->dada;
            if (ant==NULL)
                a_inici = ant = nou;
            else
            {
                ant->seg = nou;
                ant = nou;
            }
            p=p->seg;
        }
        if (ant!=NULL)
            ant->seg = NULL;
        else
            a_inici = NULL;
    }

    void alliberar()
    {
// Pre: --; Post: llista buida
        Node *p;
        while ( a_inici != NULL )
        {
            p = a_inici;
            a_inici = a_inici->seg;
            delete p;
        }
    }
};
#endif
