#ifndef PUNT3D_H
#define PUNT3D_H


class Punt3D
{
    public:
        Punt3D();
        Punt3D(double x, double y, double z);
        void llegir();
        void mostrar() const;
        void Resta(Punt3D p);
        void Suma(Punt3D p);
        void MultiplicacioReal(double r);
        void ProducteVectorial(Punt3D p);
        bool Emisferi() const;



    protected:

    private:
        double a_x, a_y, a_z;
};

#endif // PUNT3D_H
