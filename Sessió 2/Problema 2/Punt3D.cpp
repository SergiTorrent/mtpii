#include "Punt3D.h"
#include <iostream>

using namespace std;

Punt3D::Punt3D()
{
    a_x=0;
    a_y=0;
    a_z=0;
}
Punt3D::Punt3D(double x, double y, double z)
{
    a_x=x;
    a_y=y;
    a_z=z;
}
void Punt3D::llegir()
{
    cin>>a_x>>a_y>>a_z;
}
void Punt3D::mostrar() const
{
    cout<<"RESULTAT: ("<<a_x<<", "<<a_y<<", "<<a_z<<")"<<endl;

}
void Punt3D::Resta(Punt3D p)
{
    a_x-=p.a_x;
    a_y-=p.a_y;
    a_z-=p.a_z;
}
void Punt3D::Suma(Punt3D p)
{
    a_x+=p.a_x;
    a_y+=p.a_y;
    a_z+=p.a_z;
}
void Punt3D::MultiplicacioReal(double r)
{
    a_x*=r;
    a_y*=r;
    a_z*=r;
}
void Punt3D::ProducteVectorial(Punt3D p)
{
    double x,y,z;
    x= (a_y*p.a_z)-(a_z*p.a_y);
    y= (a_z*p.a_x)-(a_x*p.a_z);
    z= (a_x*p.a_y)-(a_y*p.a_x);
    a_x=x;
    a_y=y;
    a_z=z;
}
bool Punt3D::Emisferi() const
{
    return a_z>0;
}
