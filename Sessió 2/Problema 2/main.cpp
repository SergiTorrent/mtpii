#include <iostream>
#include "Punt3D.h"

using namespace std;

int main()
{
    cout.setf(ios::fixed);
    cout.precision (2);
    int comptador=0, emisferi=0;
    char operacio='z';
    double real;
    double x,y,z;
    cout << "ENTRAR COMPONENT X:" << endl;
    cin >> x;
    cout << "ENTRAR COMPONENT Y:" << endl;
    cin >> y;
    cout << "ENTRAR COMPONENT Z:" << endl;
    cin >> z;
    Punt3D PuntA(x,y,z);
    while (operacio != 'f' and comptador<5)
    {
        cout <<"ENTRAR OPERACIO (s, r, m, v, f):"<<endl;
        cin >> operacio;
        if (operacio == 's')
        {
            cout << "ENTRAR COMPONENT X:" << endl;
            cin >> x;
            cout << "ENTRAR COMPONENT Y:" << endl;
            cin >> y;
            cout << "ENTRAR COMPONENT Z:" << endl;
            cin >> z;
            Punt3D Auxiliar(x,y,z);
            PuntA.Suma(Auxiliar);
            PuntA.mostrar();
            comptador++;
            if (PuntA.Emisferi()){
            emisferi++;
        }
        }
        else if (operacio == 'r')
        {
            cout << "ENTRAR COMPONENT X:" << endl;
            cin >> x;
            cout << "ENTRAR COMPONENT Y:" << endl;
            cin >> y;
            cout << "ENTRAR COMPONENT Z:" << endl;
            cin >> z;
            Punt3D Auxiliar(x,y,z);
            PuntA.Resta(Auxiliar);
            PuntA.mostrar();
            comptador++;
            if (PuntA.Emisferi()){
            emisferi++;
        }
        }
        else if (operacio == 'm')
        {
            cout << "ENTRAR UN VALOR REAL:" << endl;
            cin >> real;
            PuntA.MultiplicacioReal(real);
            PuntA.mostrar();
            comptador++;
            if (PuntA.Emisferi()){
            emisferi++;
        }
        }
        else if (operacio == 'v')
        {
            cout << "ENTRAR COMPONENT X:" << endl;
            cin >> x;
            cout << "ENTRAR COMPONENT Y:" << endl;
            cin >> y;
            cout << "ENTRAR COMPONENT Z:" << endl;
            cin >> z;
            Punt3D Auxiliar(x,y,z);
            PuntA.ProducteVectorial(Auxiliar);
            PuntA.mostrar();
            comptador++;
            if (PuntA.Emisferi()){
            emisferi++;
        }
        }

    }
    cout << "NOMBRE D'OPERACIONS: "<< comptador<<endl;
    cout << "RESULTATS A L'HEMISFERI NORD: "<< emisferi<<endl;

    return 0;
}
