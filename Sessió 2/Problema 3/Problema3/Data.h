#ifndef DATA_H
#define DATA_H


class Data
{
public:
    Data();
    Data(int d, int m, int a);
    void llegir1();
    void llegir2();
    void OrdenarCurt();
    void OrdenarLlarg();


protected:

private:
    unsigned ndata=0;
    struct SetData {
        int dia;
        int mes;
        int any;
    };
    typedef SetData TaulaDates[101];
    TaulaDates dates;

    bool esDeTraspas(unsigned any)
    {
        return (!(any%4) && (any%100) || !(any%400));
    }


    bool esValida(unsigned any,unsigned mes,unsigned dia)
    {
        unsigned mesllarg[]= {31,28,31,30,31,30,31,31,30,31,30,31};
        if (!any || !mes || !dia || mes>12)
            return 0;
        if (esDeTraspas(any) && mes==2)
            mesllarg[1]++;
        if (dia>mesllarg[mes-1])
            return 0;
        return 1;
    }
};

#endif // DATA_H
