#include "Data.h"
#include <iostream>

using namespace std;

Data::Data()
{
    dates[ndata].dia=1;
    dates[ndata].mes=1;
    dates[ndata].any=2001;
}
Data::Data(int d, int m, int a)
{
    ndata++;
    dates[ndata].dia=d;
    dates[ndata].mes=m;
    dates[ndata].any=a;

}
void Data::llegir1()
{
    ndata++;
    int d;
    cin>>d;
    dates[ndata].dia = d % 100;
    d = d/100;
    dates[ndata].mes = d % 100;
    d = d/100;
    dates[ndata].any = d;
    while (!esValida(dates[ndata].any,dates[ndata].mes,dates[ndata].dia)){
        cin >> d;
        dates[ndata].dia = d % 100;
        d = d/100;
        dates[ndata].mes = d % 100;
        d = d/100;
        dates[ndata].any = d;
    }
}
void Data::llegir2()
{
    ndata++;
    cin>>dates[ndata].dia>>dates[ndata].mes>>dates[ndata].any;
    while (!esValida(dates[ndata].any,dates[ndata].mes,dates[ndata].dia)){
        cin>>dates[ndata].dia>>dates[ndata].mes>>dates[ndata].any;
    }

}
void Data::OrdenarCurt()
{
    unsigned aux=ndata;
    while (ndata != 0)
        {
            cout<<dates[ndata].dia<<"/"<<dates[ndata].mes<<"/"<<dates[ndata].any<<endl;
            ndata--;
        }
    ndata=aux;
}
void Data::OrdenarLlarg()
{
    unsigned aux=ndata;
    while (ndata!=0){
    string m;
    cout<<dates[ndata].dia<<"-";
    if (dates[ndata].mes == 1) { m = "GENER";}
    else if (dates[ndata].mes == 2) { m = "FEBRER";}
    else if (dates[ndata].mes == 3) { m = "MAR�";}
    else if (dates[ndata].mes == 4) { m = "ABRIL";}
    else if (dates[ndata].mes == 5) { m = "MAIG";}
    else if (dates[ndata].mes == 6) { m = "JUNY";}
    else if (dates[ndata].mes == 7) { m = "JULIOL";}
    else if (dates[ndata].mes == 8) { m = "AGOST";}
    else if (dates[ndata].mes == 9) { m = "SETEMBRE";}
    else if (dates[ndata].mes == 10) { m = "OCTUBRE";}
    else if (dates[ndata].mes == 11) { m = "NOVEMBRE";}
    else if (dates[ndata].mes == 12) { m = "DESEMBRE";}
    else { m= "INDEFINIT";}
    cout<<m<<"-";
    cout<<dates[ndata].any<<endl;
    ndata--;
    }
    ndata=aux;
}
