#include <iostream>
#include "Data.h"

using namespace std;

int main()
{
    Data data;
    int opcio;
    cout << "OPCIONS:" << endl;
    cout << "1: ENTRAR UNA DATA EN FORMAT AAAAMMDD" << endl;
    cout << "2: ENTRAR UNA DATA EN FORMAT D M A" << endl;
    cout << "3: MOSTRAR LES DATES ORDENADES EN FORMAT CURT" << endl;
    cout << "4: MOSTRAR LES DATES ORDENADES EN FORMAT LLARG" << endl;
    cout << "9: MOSTRAR EL MENU D'OPCIONS" << endl;
    cout << "0: SORTIR DEL PROGRAMA" << endl;
    cout << "OPCIO:" << endl;
    cin>>opcio;
    while (opcio!=0)
    {
        if (opcio==1)
        {
            cout<<"ENTRA UNA DATA (AAAAMMDD):"<<endl;
            data.llegir1();
        }
        else if (opcio==2)
        {
            cout<<"ENTRA UNA DATA (D M A):"<<endl;
            data.llegir2();
        }
        else if (opcio==3)
        {
            cout<<"LLISTAT DE LES DATES:"<<endl;
            data.OrdenarCurt();
        }
        else if (opcio==4)
        {
            cout<<"LLISTAT DE LES DATES:"<<endl;
            data.OrdenarLlarg();
        }
        else if (opcio==9)
        {
            cout << "OPCIONS:" << endl;
            cout << "1: ENTRAR UNA DATA EN FORMAT AAAAMMDD" << endl;
            cout << "2: ENTRAR UNA DATA EN FORMAT D M A" << endl;
            cout << "3: MOSTRAR LES DATES ORDENADES EN FORMAT CURT" << endl;
            cout << "4: MOSTRAR LES DATES ORDENADES EN FORMAT LLARG" << endl;
            cout << "9: MOSTRAR EL MENU D'OPCIONS" << endl;
            cout << "0: SORTIR DEL PROGRAMA" << endl;
        }
        cout << "OPCIO:" << endl;
        cin>>opcio;
    }
    return 0;
}
