#include <iostream>
#include "Punt2D.h"

using namespace std;

int main()
{
    cout.setf(ios::fixed);
    cout.precision (2);
    double distancia = 0;
    int costats = 1;
    cout << "Entra els punts (per acabar, torna a entrar el primer):" << endl;
    Punt2D primer,seguents, anterior;
    primer.llegir();
    seguents.llegir();
    anterior = seguents;
    distancia = primer.distancia(seguents);
    while (not (primer.es_igual(seguents))) {
        seguents.llegir();
        distancia = seguents.distancia(anterior) + distancia;
        costats++;
        anterior = seguents;
    }
    cout <<"El pol�gon t� "<<costats<<" costats i el seu per�metre �s "<<distancia;
    return 0;
}
