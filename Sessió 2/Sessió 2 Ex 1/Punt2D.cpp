#include <iostream>
#include <cmath>
#include "Punt2D.h"
using namespace std;
// Constructors
Punt2D::Punt2D()
{
// Pre:--; Post: el punt �s (0,0)
// constructor per defecte, usat en declaracions Punt2D p;
    a_x = 0;
    a_y = 0;
}
Punt2D::Punt2D(double x, double y)
{
// Pre:--; Post: el punt �s (x,y)
// constructor amb par�metres
// usat en declaracions Punt2D p1 = Punt2D(2.3, 4.5);
// o Punt2D p2(6.7, 8.9);
    a_x = x;
    a_y = y;
}

// Consultors
double Punt2D::coordenada_x() const
{
//Pre: --; Post: retorna la coordenada x del punt
    return a_x;
}
double Punt2D::coordenada_y() const
{
//Pre: --; Post: retorna la coordenada y del punt
    return a_y;
}
double Punt2D::distancia(Punt2D p) const
{
//Pre: --; Post: retorna la dist�ncia del punt a p
    double difX = a_x-p.a_x;
    double difY = a_y-p.a_y;
    return sqrt(difX*difX+difY*difY);
}

void Punt2D::mostrar() const
{
//Pre: --; Post: mostra el punt en forma (x,y)
    cout << "(" << a_x << "," << a_y << ")" << endl;
}
bool Punt2D::es_igual(Punt2D p) const
{
//: --; Post: retorna cert si el punt i p s�n iguals
    return a_x == p.a_x and a_y == p.a_y;
}
Punt2D Punt2D::punt_mig(Punt2D p) const
{
//Pre:--; Post: retorna el punt mig entre el punt i p
    return Punt2D((a_x+p.a_x)/2, (a_y+p.a_y)/2);
}

void Punt2D::moure(double x, double y)
{
//Pre:--; Post: punt despla�at amb vector (x,y)
    a_x += x;
    a_y += y;
}
void Punt2D::moure(Punt2D p)
{
//Pre:--; Post: punt despla�at amb vector (0,0)->p
    a_x += p.a_x;
    a_y += p.a_y;
}
void Punt2D::llegir()
{
//Pre:--; Post: el punt �s el llegit de teclat
    cin >> a_x >> a_y;
}
