//
//

#ifndef DATA_H
#define DATA_H
#include <iomanip>
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

class Data
{
public:
//    Data();
    Data(unsigned dia= 1, unsigned mes= 1, unsigned any=1999);
    void llegirDatafitxer(ifstream& f);
    void igualar(Data d);
    bool esMenor(Data d);
    void llegirData();
    void mostrar() const;
    int Dia();
    bool esIgual(Data d);

protected:

private:
    unsigned any;
    unsigned mes;
    unsigned dia;
    string data;

    bool esDeTraspas(unsigned any)
    {
        return (!(any%4) && (any%100) || !(any%400));
    }


    bool esValida(unsigned any,unsigned mes,unsigned dia)
    {
        unsigned mesllarg[]= {31,28,31,30,31,30,31,31,30,31,30,31};
        if (!any || !mes || !dia || mes>12)
            return 0;
        if (esDeTraspas(any) && mes==2)
            mesllarg[1]++;
        if (dia>mesllarg[mes-1])
            return 0;
        return 1;
    }

int ConvertirStringInt(const string& input)
{
    stringstream ss(input);
    int retval;
    ss >> retval;
    return retval;
}


void convertStrtoArrData(string str, int &dia, int &mes, int &any)
{
    // get length of string str
    int str_length = str.length();

    // create an array with size as string
    // length and initialize with 0
    int arr[str_length] = { 0 };

    int j = 0, i, sum = 0;

    // Traverse the string
    for (i = 0; str[i] != '\0'; i++) {

        // if str[i] is ', ' then split
        if (str[i] == ' ') {

            // Increment j to point to next
            // array location
            j++;
        }
        else {

            // subtract str[i] by 48 to convert it to int
            // Generate number by multiplying 10 and adding
            // (int)(str[i])
            arr[j] = arr[j] * 10 + (str[i] - 48);
        }
    }
    dia=arr[0];
    mes=arr[1];
    any=arr[2];
}
};



#endif //PROJECTEPROBLEMA2_DATA_H
