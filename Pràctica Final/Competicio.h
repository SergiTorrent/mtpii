#ifndef COMPETICIO_H
#define COMPETICIO_H
#include "Equip.h"
#include "Partit.h"
#include "Data.h"
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>



class Competicio
{
    public:
        Competicio();
        void llegir_fitxer(ifstream& f);
        int ConvertirStringInt(const string& input);
        void classificacio();
        void llistat_per_punts_a_favor();
        void mostrar_partits_diferencia_minima(int diferencia);
        void quicksortData(int inici, int fi);
        int dividirData(int inici, int fi);
        void FerLlistaEquips(Partit pa);
        void FerLlistaEquips();


    protected:

    private:
        Partit p[380];
        int cont;
        Equip e[20];
        int conte;
};

#endif // COMPETICIO_H
