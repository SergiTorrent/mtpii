//
//  main.cpp
//  Eurolliga
//
//

#ifdef _WIN32
#include<windows.h>
#endif

#include <iostream>
#include <fstream>
#include <string>
#include "Competicio.h"
using namespace std;

void menu() {
    // Pre: --; Post: mostra el men� de comandes

    cout << endl << "ENTRA COMANDA:" << endl;
    cout << "MC (classificacio)" << endl;
    cout << "LP (llistat per punts a favor)" << endl;
    cout << "GDP num_punts (llistar partits amb diferencia >= a num_punts)" << endl;
    cout << "XX (acabar)" << endl << endl << "@ " << endl;
}

void processar_comandes(Competicio & competicio) {
/* Pre: --
   Post: processa les seg�ents comandes d'usuari, modificant competicio quan cal:

   MC: Llistat d'equips ordenat per
     (1) partits guanyats (decre)
     (2) difer�ncia punts a favor - punts en contra (decre)
     (3) punts a favor  (decre)
     (4) ordre alfab�tic
     Format: nom_equip guanyats perduts punts_favor punts_contra

   LP: Llistat d'equips ordenats per (1) punts a favor (decre) (2) ordre alfab�tic (mateix format que l'anterior llistat)

   GDP num_punts: mostrar els partits on la difer�ncia de punts sigui >= a num_punts
     --> si n'hi ha: els mostrem ordenats per
     (1) data (creixent)
     (2) nom equip local (alfab�tic)
     --> si no n'hi ha cap: TOTS ELS PARTITS TENEN DIFERENCIES MENORS A num_punts

   XX: acabar
 */

    menu();
    string comanda;
    cin >> comanda;
    string nom_equip;

    while (comanda != "XX") {
        if (comanda == "MC") competicio.classificacio();
        else if (comanda == "LP") competicio.llistat_per_punts_a_favor();
        else if (comanda=="GDP") {
            int diferencia;
            cin >> diferencia;
            competicio.mostrar_partits_diferencia_minima(diferencia);
        }

        menu();
        cin >> comanda;
    }
}

int main() {
#ifdef _WIN32
    SetConsoleOutputCP(1252);
    SetConsoleCP(1252);
#endif

    cout << "INTRODUEIX NOM FITXER:" << endl;
    string nom_fitxer;
    cin >> nom_fitxer;
    ifstream f;
    f.open(nom_fitxer.c_str());
    if (not f.is_open()) cout << "ERROR OBERTURA FITXER" << endl;
    else {
        Competicio competicio;
        competicio.llegir_fitxer(f);
        competicio.FerLlistaEquips();
        processar_comandes(competicio);
        competicio.FerLlistaEquips();
    }
    f.close();

    return 0;
}



