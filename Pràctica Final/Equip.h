#ifndef EQUIP_H
#define EQUIP_H
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

class Equip
{
    public:
        Equip();
        void mostrar() const;
        string Nom();

    protected:

    private:
        string nom;
        int guanyats,perduts,punts_favor,punts_contra;
};

#endif // EQUIP_H
