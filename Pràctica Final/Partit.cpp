#include "Partit.h"

Partit::Partit()
{

}

Partit::Partit(Data d,string l, string v, int pl, int pv)
{
    data=d;
    local=l;
    visitant=v;
    punts_local=pl;
    punts_visitant=pv;
}

Partit Partit::llegir_partit(ifstream& f)
{
      data.llegirDatafitxer(f);
      getline(f,local);
      string puntuacio;
      getline(f,puntuacio);
      int pl,pv;
      if (!f.eof()){
      convertStrtoArrPuntuacio(puntuacio,pl,pv);
      punts_local=pl;
      punts_visitant=pv;
      getline(f,visitant);
//    getline(f,local);
//    f>>punts_local;
//    f>>punts_visitant;
 //   getline(f,visitant);
      }
    Partit act(data,local,visitant,punts_local,punts_visitant);
    return act;
}

 void Partit::mostrar() {
    data.mostrar();
    cout << setw(18) << local << setw(4) << punts_local << setw(5) << punts_visitant << "  " << left <<setw(18) << visitant << endl;
 }
 string Partit::Local()
 {
     return local;
 }

 bool Partit::DataMesPetita(Partit pibot)
{
    return data.esMenor(pibot.data);
}

bool Partit::DataIgual(Partit pibot)
{
    return data.esIgual(pibot.data);
}
