#ifndef PARTIT_H
#define PARTIT_H
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include "Data.h"

using namespace std;

class Partit
{
    public:
        Partit();
        Partit(Data d,string l, string v, int pl, int pv);
        Partit llegir_partit(ifstream& f);
        void mostrar();
        string Local();
        bool DataMesPetita(Partit p);
        bool DataIgual(Partit p);
    protected:

    private:
        Data data;
        string local,visitant;
        int punts_local, punts_visitant;

void convertStrtoArrPuntuacio(string str, int &plocal, int &pvisitant)
{
    // get length of string str
    int str_length = str.length();

    // create an array with size as string
    // length and initialize with 0
    int arr[str_length] = { 0 };

    int j = 0, i, sum = 0;

    // Traverse the string
    for (i = 0; str[i] != '\0'; i++) {

        // if str[i] is ', ' then split
        if (str[i] == ' ') {
            // Increment j to point to next
            // array location
            j++;
        }
        else {

            // subtract str[i] by 48 to convert it to int
            // Generate number by multiplying 10 and adding
            // (int)(str[i])
            arr[j] = arr[j] * 10 + (str[i] - 48);
        }
    }
    plocal=arr[0];
    pvisitant=arr[2];
}


int ConvertirStringInt(const string& input)
{
    stringstream ss(input);
    int retval;
    ss >> retval;
    return retval;
}


};

#endif // PARTIT_H
