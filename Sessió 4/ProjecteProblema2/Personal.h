//
//

#ifndef PERSONAL_H
#define PERSONAL_H


#include "Empleat.h"


class Personal {
    static const int MAX = 50;
public:
    Personal();

    void afegir(Empleat e);

    void llistarEmpleats() const;

    void llistarEmpleatsPoblacio(string poblacio) const;

protected:

private:
    typedef Empleat TaulaE[MAX];
    TaulaE a_t;
    int a_n;

    int dividir(int inicio, int fin);

    void quicksort(int inicio, int fin);

};


#endif //PROJECTEPROBLEMA2_PERSONAL_H
