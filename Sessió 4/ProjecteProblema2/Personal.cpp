#include "Personal.h"
#include "Empleat.h"
#include <iostream>

Personal::Personal()
{
    a_n=0;
    Empleat a_t[0];
}
void Personal::afegir(Empleat e)
{
    a_t[a_n]=e;
    a_n++;
    quicksort(0, a_n - 1);
}
void Personal::llistarEmpleats() const
{
    for (int i = 0; i < a_n; i++)
        a_t[i].mostrarEmpleat();
}
void Personal::llistarEmpleatsPoblacio(string poblacio) const
{
    for (int i = 0; i < a_n; i++)
        if (a_t[i].Poblacio()==poblacio)
            a_t[i].mostrarEmpleat();
}

void Personal::quicksort(int inicio, int fin)
{
    int pivote;
    if(inicio < fin)
    {
        pivote = dividir(inicio, fin);
        quicksort(inicio, pivote - 1 );//ordeno la lista de los menores
        quicksort(pivote + 1, fin );//ordeno la lista de los mayores
    }
}

int Personal::dividir(int inicio, int fin)
{
    int izq;
    int der;
    Empleat pibote;
    Empleat temp;

    pibote = a_t[inicio];
    izq = inicio;
    der = fin;

    //Mientras no se cruzen los índices
    while (izq < der){
        while (a_t[der].Codi() > pibote.Codi()){
            der--;
        }

        while ((izq < der) && (a_t[izq].Codi() <= pibote.Codi())){
            izq++;
        }

        // Si todavia no se cruzan los indices seguimos intercambiando
        if(izq < der){
            temp= a_t[izq];
            a_t[izq] = a_t[der];
            a_t[der] = temp;
        }
    }

    //Los indices ya se han cruzado, ponemos el pivote en el lugar que le corresponde
    temp = a_t[der];
    a_t[der] = a_t[inicio];
    a_t[inicio] = temp;

    //La nueva posición del pivote
    return der;
}