#include "Ingredient.h"
#include <iostream>
#include <string>

using namespace std;

Ingredient::Ingredient()
{
    nomi="res";
    quanti=0;
}
//Ingredient::Ingredient(string &n, double &q)
//{
//    nomi=n;
//    quanti=q;
//}

string Ingredient::NomIngredient() const
{
    return nomi;
}
double Ingredient::QuantitatIngredient() const
{
    return quanti;
}
bool Ingredient::CompararIngredients(Ingredient i) const
{
    return nomi==i.nomi;
}
bool Ingredient::CompararQuantitats(Ingredient i) const
{
    return quanti==i.quanti;
}
double Ingredient::DifIngredients (Ingredient i) const
{
    return quanti-i.quanti;
}

void Ingredient::mostrar() const
{
    cout<<nomi<<" "<<quanti<<endl;
}
void Ingredient::GuardarIngredient(string &n, double &q)
{
    nomi=n;
    quanti=q;
}
bool Ingredient::llegir()
{
    cin>>nomi;
    if (nomi!="#")
    {
    cin>>quanti;
    return true;
    }
    else return false;
}
