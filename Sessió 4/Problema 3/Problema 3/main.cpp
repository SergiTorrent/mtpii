#include <iostream>
#include <fstream>
#include <string>
#include "Recepta.h";

using namespace std;

int main()
{
    string nomfitxer;
    cout << "Entra el nom del fitxer amb la recepta familiar:" << endl;
    cin>> nomfitxer;
    ifstream f;
    f.open(nomfitxer.c_str());
//    FitxerReceptes.open("nomfitxer.txt");
    if (f.is_open())
    {
        Recepta ra;
        Recepta rf;
        while (!f.eof())
        {
            string nom;
            double quantitat;
            f>>nom;
            f>>quantitat;
            if (nom!="#" && nom!="")
            rf.llegirFitxer(nom, quantitat);
        }
        cout<<"Entra els ingredients de la recepta a millorar (# per acabar):"<<endl;
        Ingredient i;
        bool continuar=i.llegir();
        while (continuar)
        {
            ra.llegirTeclat(i);
            continuar=i.llegir();
        }
        //ara que hem llegit els ingrdients els comparem
        rf.CompararReceptes(ra);
    }
    else cout<<"No s'ha trobat el fitxer";
    return 0;
}
