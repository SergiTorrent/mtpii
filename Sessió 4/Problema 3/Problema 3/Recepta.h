#ifndef RECEPTA_H
#define RECEPTA_H
#include "Ingredient.h"
#include <string>
#include <iostream>

using namespace std;

class Recepta
{
    public:
        Recepta();
        void llegirFitxer(string nom, double quantitat);
        void llegirTeclat(Ingredient i);
        void CompararReceptes(const Recepta &a) const;
        void mostrar() const;
        bool FinalRecepta() const;
        bool existeix(string c);

    protected:

    private:
        Ingredient i_t[500];
        int i_n=0;

        int dividir(int inicio, int fin);

        void quicksort(int inicio, int fin);
};

#endif // RECEPTA_H
