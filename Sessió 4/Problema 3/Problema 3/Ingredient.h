#ifndef INGREDIENT_H
#define INGREDIENT_H
#include <string>
#include <iostream>



using namespace std;
class Ingredient
{
    public:
        Ingredient();
//        Ingredient(string &n, double &q);
        string NomIngredient() const;
        bool llegir();
        double QuantitatIngredient() const;
        bool CompararIngredients(Ingredient i) const;
        bool CompararQuantitats(Ingredient i) const;
        double DifIngredients (Ingredient i) const;
        void mostrar() const;
        void GuardarIngredient(string &n, double &q);


    protected:

    private:
        string nomi;
        double quanti;
};

#endif // INGREDIENT_H
