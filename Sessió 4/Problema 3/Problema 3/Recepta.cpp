#include "Recepta.h"
#include "Ingredient.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

Recepta::Recepta()
{

}
void Recepta::llegirFitxer(string nom,double quantitat)
{
    i_t[i_n].GuardarIngredient(nom,quantitat);
    i_n++;
    quicksort(0, i_n - 1);
}
void Recepta::llegirTeclat(Ingredient i)
{
    if (i.NomIngredient()!="#")
    {
        i_t[i_n]=i;
        i_n++;
        quicksort(0, i_n - 1);
    }

}
void Recepta::CompararReceptes(const Recepta &a) const
{
    Ingredient s;
    for (int i=0;i<i_n;i++)
    {
    bool trobatIngredient=false;
        for (int j=0;j<a.i_n;j++)
        {
            if (i_t[i].CompararIngredients(a.i_t[j]) && !i_t[i].CompararQuantitats(a.i_t[j]))
            {
                if (i_t[i].DifIngredients(a.i_t[j])>0) cout<<"+ FALTA: "<<i_t[i].NomIngredient()<<" "<<i_t[i].DifIngredients(a.i_t[j])<<endl; //1
                else if (i_t[i].DifIngredients(a.i_t[j])<0) cout<<"- SOBRA: "<<i_t[i].NomIngredient()<<" "<<i_t[i].DifIngredients(a.i_t[j])*(-1)<<endl; //2
            }
            if (i_t[i].CompararIngredients(a.i_t[j])) trobatIngredient=true;
        }
    if (!trobatIngredient)
    {
        cout<<"+ FALTA: "; //3
        i_t[i].mostrar();
    }
    }
    for (int i=0;i<a.i_n;i++)
    {
    bool Familiar=false;
        for (int j=0;j<i_n;j++)
        {
        if (a.i_t[i].CompararIngredients(i_t[j])) Familiar=true;
        }
    if (!Familiar)
    {
        cout<<"+ SOBRA: "; //4
        a.i_t[i].mostrar();
    }
    }

}

bool Recepta::FinalRecepta() const
{
    return i_t[i_n].NomIngredient()=="#";
}

void Recepta::mostrar() const
{
    for (int i=0;i<i_n;i++)
    i_t[i].mostrar();
}

void Recepta::quicksort(int inicio, int fin)
{
    int pivote;
    if(inicio < fin)
    {
        pivote = dividir(inicio, fin);
        quicksort(inicio, pivote - 1 );//ordeno la lista de los menores
        quicksort(pivote + 1, fin );//ordeno la lista de los mayores
    }
}

int Recepta::dividir(int inicio, int fin)
{
    int izq;
    int der;
    Ingredient pibote;
    Ingredient temp;

    pibote = i_t[inicio];
    izq = inicio;
    der = fin;

    //Mientras no se cruzen los �ndices
    while (izq < der){
        while (i_t[der].NomIngredient() > pibote.NomIngredient()){
            der--;
        }

        while ((izq < der) && (i_t[izq].NomIngredient() <= pibote.NomIngredient())){
            izq++;
        }

        // Si todavia no se cruzan los indices seguimos intercambiando
        if(izq < der){
            temp= i_t[izq];
            i_t[izq] = i_t[der];
            i_t[der] = temp;
        }
    }

    //Los indices ya se han cruzado, ponemos el pivote en el lugar que le corresponde
    temp = i_t[der];
    i_t[der] = i_t[inicio];
    i_t[inicio] = temp;

    //La nueva posici�n del pivote
    return der;
}

bool Recepta::existeix(string c)
{
    bool trobat=false;
    for (int i = 0; i<i_n; i++) if ((i_t[i].NomIngredient())==c) trobat=true;
    return trobat;
}

