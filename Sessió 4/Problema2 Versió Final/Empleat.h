//
//

#ifndef EMPLEAT_H
#define EMPLEAT_H

#include "Data.h"
#include <iostream>

using namespace std;


class Empleat
{
public:
    Empleat();
//        Empleat(unsigned c, string n, string c, string p, Data d);
    void MesPetit(Empleat e);
    bool llegir();
    void llegire();
    bool finalitzacio();
    void igualar(Empleat e);
    void mostrarEmpleat() const;
    bool CodiMenor(Empleat e);
    void Intercanviar(Empleat e);
    string Poblacio() const;
    bool comprovarCodi();
    unsigned Codi();

protected:

private:
    unsigned codi;
    string nom;
    string cognom;
    string poblacio;
    Data data;
};


#endif //PROJECTEPROBLEMA2_EMPLEAT_H
