#include "Empleat.h"
#include "Data.h"
#include <iostream>
#include <string>

using namespace std;

Empleat::Empleat()
{
    codi=1;
    nom="a";
    cognom="b";
    poblacio="c";
    data;
}
//Empleat::Empleat(unsigned c, string n, string c, string p, Data d)
//{
//    codi=c;
//    nom=n;
//    cognom=c;
//    poblacio=p;
//    data.igualar(d)
//}
void Empleat::MesPetit(Empleat e)
{
    if (e.codi!=0){
    if (!e.data.esMenor(data))
    {
        codi=e.codi;
        nom=e.nom;
        cognom=e.cognom;
        poblacio=e.poblacio;
        data.igualar(e.data);
    }
    }
}
void Empleat::llegire()
{
    cout<<"CODI:"<<endl;
    cin>>codi;
    if (codi!=0){
    cout<<"NOM:"<<endl;
    cin>>nom;
    cout<<"COGNOM:"<<endl;
    cin>>cognom;
    cout<<"POBLACIO:"<<endl;
    cin>>poblacio;
    cout<<"DATA DE NAIXEMENT (AAAAMMDD):"<<endl;
    data.llegirData();
    }
}

bool Empleat::finalitzacio()
{
    return codi==0;
}
void Empleat::igualar(Empleat e)
{
    codi=e.codi;
    nom=e.nom;
    cognom=e.cognom;
    poblacio=e.poblacio;
    data.igualar(e.data);
}

void Empleat::mostrarEmpleat() const
{
    //cout<<"EL MES JOVE ES:"<<endl;
    //cout<<"COD: "<<codi<<endl;
    //cout<<"NOM: "<<nom<<endl;
    //cout<<"COG: "<<cognom<<endl;
    //cout<<"POB: "<<poblacio<<endl;
    //cout<<"D.N: ";
    //data.mostrarData();
    //cout<<endl;

    cout <<codi<<" "<<nom<<" "<<cognom<<" "<<poblacio<<" ";
    data.mostrarData();
}

bool Empleat::CodiMenor(Empleat e)
{
    return codi<e.codi;
}
void Empleat::Intercanviar(Empleat e)
{
    int auxcodi;
    string auxnom;
    string auxcognom;
    string auxpoblacio;
    Data auxdata;

    auxcodi=codi;
    auxnom=nom;
    auxcognom=cognom;
    auxpoblacio=poblacio;
    auxdata.igualar(data);

    codi=e.codi;
    nom=e.nom;
    cognom=e.cognom;
    poblacio=e.poblacio;
    data.igualar(e.data);

    e.codi=auxcodi;
    e.nom=auxnom;
    e.cognom=auxcognom;
    e.poblacio=auxpoblacio;
    data.igualar(auxdata);
}

string Empleat::Poblacio() const
{
    return poblacio;
}
bool Empleat::comprovarCodi()
{
    return codi==0;
}
bool Empleat::llegir()
{
    cout<<"CODI:"<<endl;
    cin>>codi;
    if (codi!=0){
    cout<<"NOM:"<<endl;
    cin>>nom;
    cout<<"COGNOM:"<<endl;
    cin>>cognom;
    cout<<"POBLACIO:"<<endl;
    cin>>poblacio;
    cout<<"DATA DE NAIXEMENT (AAAAMMDD):"<<endl;
    data.llegirData();
    }
    return codi!=0;
}
