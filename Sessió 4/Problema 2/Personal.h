#ifndef PERSONAL_H
#define PERSONAL_H
#include "Empleat.h"

using namespace std;


class Personal
{
    public:
        Personal();
        afegir(Empleat em);
        void llistarEmpleats() const;
        void llistarEmpleatsPoblacio(string poblacio) const;
        void Ordenar();


    protected:

    private:
        //struct Empleats
        //{
        //    Empleat em;
        //    int n_empleat=0;
        //};
        //typedef Empleats TaulaE[50];
        //TaulaE a;

        typedef Empleat TaulaE [50];
        struct TaulaEmpleats
        {
            TaulaE empleats;
            int n_empleats = 0;
        };
        TaulaEmpleats grup;


//    void ordenaCodi(TaulaEmpleats &v)
//    {
//Pre:	0<=v.n<=MAX
//Post:	v.t[0..v.n-1]	cont�	els	elements	inicials	en	ordre	creixent
//        for(int i=0; i<v.n_empleats-1; i++)
//        {
//            for(int j=v.n_empleats-1; j>i; j--)
//            {
//                if(esMenorCodi(v.empleats[j],v.empleats[j-1])){
//                    intercanvi(v.empleats[j-1],v.empleats[j]);
//                }
//                }
//        }
//    }


//    bool esMenorCodi(Empleat a, Empleat b)
//    {
//        return a.CodiMenor(b);
//    }

//    void intercanvi(Empleat a, Empleat b)
//    {
//        a.Intercanviar(b);
//    }
//};

    void ordena(TaulaE &v)
    {
        //Pre:	0<=v.n<=MAX
        //Post:	v.t[0..v.n-1]	cont�	els	elements	inicials	en	ordre	creixent.
        for(int i=0; i<v.n_empleats-1; i++)
        {
            int pm = posicioMin(v,i);
            intercanvi(v.empleats[i],v.empleats[pm]);
        }
    }
    int posicioMin(TaulaE v, int i)
    {
//Pre:	0<=v.n<=MAX,	0<=i<v.n
//Post:	retorna	pos.	del	m�nim	de	v.t[i..v.n-1]
        int m=i;
        for(int j=i+1; j<v.n_empleats; j++)
        {
            if(esMenor(v.empleats[j],v.empleats[m]) m=j;
        }
    return m;
}
    void intercanvi(Empleat a, Empleat b)
    {
        a.Intercanviar(b);
    }
};

#endif // PERSONAL_H
