/****************************************************
 * Codi de l'exercici Llistat d'empleats
 *
 * Falta l'especificaci� del programa
 ****************************************************/

#include <iostream>
#include "Personal.h" // Ull amb el nom del fitxer
#include "Empleat.h"  // Ull amb el nom del fitxer

using namespace std;


void afegirEmpleat1(Personal & p) {
// Pre: cert
// Post: s'ha afegit un empleat (si no �s repetit)
    Empleat e;

    cout << "ENTRA UN EMPLEAT:" << endl;
    e.llegir();
    p.afegir(e);
}



void afegirEmpleatN(Personal & p) {
// Pre: cert
// Post: s'afegeixen diversos empleats (si no s�n repetits)
    Empleat e;

    cout << "ENTRA UN EMPLEAT (CODI: 0 PER ACABAR):" << endl;
    bool continuar = e.llegir();
    while (continuar) {
        p.afegir(e);
        continuar = e.llegir();
    }
}



void mostrarLlistat(const Personal & p) {
// Pre: cert (el par�metre es passa per refer�ncia constant)
// Post: es fa un llistat dels empleats
    cout << "LLISTAT DELS EMPLEATS" << endl;
    p.llistarEmpleats();
}



void mostrarLlistatPoblacio(const Personal & p) {
// Pre: cert (el par�metre es passa per refer�ncia constant)
// Post: es fa un llistat dels empleats d'una poblaci� entrada per teclat
    string poblacio;
    cout << "LLISTAT DELS EMPLEATS PER POBLACIO" << endl;
    cout << "POBLACIO:" << endl;
    cin >> poblacio;
    p.llistarEmpleatsPoblacio(poblacio);
}



void menu() {
// Pre: cert
// Post: es mostren les opcions per pantalla
    cout << "OPCIONS:" << endl;
    cout << "1: ENTRAR UN EMPLEAT" << endl;
    cout << "2: ENTRAR DIVERSOS EMPLEATS" << endl;
    cout << "3: LLISTAR ELS EMPLEATS" << endl;
    cout << "4: LLISTAR ELS EMPLEATS PER POBLACIO" << endl;
    cout << "9: MOSTRAR EL MENU D'OPCIONS" << endl;
    cout << "0: SORTIR DEL PROGRAMA" << endl;
}



int main() {
    int opcio;
    Personal p;

    menu();
    cout << "OPCIO: " << endl;
    cin >> opcio;
    while (opcio != 0) {
        if (opcio == 1)
            afegirEmpleat1(p);
        else if (opcio == 2)
            afegirEmpleatN(p);
        else if (opcio == 3)
            {
            p.Ordenar();
            mostrarLlistat(p);
            }
        else if (opcio == 4)
        {
            p.Ordenar();
            mostrarLlistatPoblacio(p);
        }
        else // if (opcio == 9)
            menu();

        cout << "OPCIO: " << endl;
        cin >> opcio;
    }

    cout << "FINAL DEL PROGRAMA" << endl;

    return 0;
}

