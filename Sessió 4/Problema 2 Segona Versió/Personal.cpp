#include "Personal.h"
#include "Empleat.h"
#include <iostream>

Personal::Personal()
{
    a_n=0;
    Empleat a_t[0];
}
void Personal::afegir(Empleat e)
{
    a_t[a_n]=e;
    a_n++;
}
void Personal::llistarEmpleats() const
{
    int aux=a_n;
    while (aux>=0)
    {
        a_t[aux].mostrarEmpleat();
        aux--;
    }
}
void Personal::llistarEmpleatsPoblacio(string poblacio) const
{
    int aux=a_n;
    while (aux>=0)
    {
        if (a_t[aux].Poblacio()==poblacio) a_t[aux].mostrarEmpleat();
        aux--;
    }
}
//void Personal::ordena()
//{
//    for(int i=0; i<a_n-1; i++)
//    {
//        for(int j=a_n-1; j>i; j--)
//        {
//            if(esMenor(a_t[j],a_t[j-1]))
//                intercanvi(a_t[j-1],a_t[j]);
//            }
//    }

//}
