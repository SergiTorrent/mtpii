#include "Data.h"
#include "Empleat.h"
#include <iostream>

using namespace std;

Data::Data()
{

}
Data::Data(unsigned d, unsigned m, unsigned a)
{
    dia=d;
    mes=m;
    any=a;
}
void Data::igualar(Data d)
{
    dia=d.dia;
    mes=d.mes;
    any=d.any;
}
bool Data::esMenor(Data d)
{
    if (any>d.any){return false;}
    else if (any < d.any) {return true;}
    else
    {
        if (mes>d.mes){return false;}
        else if (mes < d.mes){return true;}
        else
        {
            if (dia>d.dia){return false;}
            else if (dia < d.dia) {return true;}
            else
                return true;
        }
    }
}
void Data::llegirData()
{
    int d;
    cin>>d;
    dia = d % 100;
    d = d/100;
    mes = d % 100;
    d = d/100;
    any = d;
    while (!esValida(any,mes,dia)){
        cin >> d;
        dia = d % 100;
        d = d/100;
        mes = d % 100;
        d = d/100;
        any = d;
    }

}

void Data::mostrarData() const
{
    string m;
    cout<<dia<<"-";
    if (mes == 1) { m = "GENER";}
    else if (mes == 2) { m = "FEBRER";}
    else if (mes == 3) { m = "MARC";}
    else if (mes == 4) { m = "ABRIL";}
    else if (mes == 5) { m = "MAIG";}
    else if (mes == 6) { m = "JUNY";}
    else if (mes == 7) { m = "JULIOL";}
    else if (mes == 8) { m = "AGOST";}
    else if (mes == 9) { m = "SETEMBRE";}
    else if (mes == 10) { m = "OCTUBRE";}
    else if (mes == 11) { m = "NOVEMBRE";}
    else if (mes == 12) { m = "DESEMBRE";}
    else { m= "INDEFINIT";}
    cout<<m<<"-";
    cout<<any<<endl;
}
