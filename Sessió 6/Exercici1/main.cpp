#include <iostream>

using namespace std;

int dividir(int inici, int fi, int a_t[])
{
    int esq;
    int dreta;
    int pibot;
    int temp;

    pibot = a_t[inici];
    esq = inici;
    dreta = fi;

    while (esq < dreta)
    {
        while ((a_t[dreta]) > pibot)
        {
            dreta--;
        }

        while ((esq < dreta) && (a_t[esq]) <= pibot)
        {
            esq++;
        }

        if(esq < dreta)
        {
            temp= a_t[esq];
            a_t[esq] = a_t[dreta];
            a_t[dreta] = temp;
        }
    }

    temp = a_t[dreta];
    a_t[dreta] = a_t[inici];
    a_t[inici] = temp;

    return dreta;
}
void quicksort(int inici, int fi, int a_t[])
{
    int pivot;
    if(inici < fi)
    {
        pivot = dividir(inici, fi, a_t);
        quicksort(inici, pivot - 1, a_t );
        quicksort(pivot + 1, fi, a_t );
    }
}



//bool existeix(int c, int nvalors, int a_t[])
//{
//    bool trobat=false;
//    for (int i = 0; i<nvalors; i++){
//            if ((a_t[i])==c) trobat=true;
//    }
//    return trobat;
//}

int main()
{
    cout << "QUANTS VALORS TENS (>0):" << endl;
    int nvalors;
    cin>>nvalors;
    while (nvalors<=0)
    {
    cout << "QUANTS VALORS TENS (>0):" << endl;
    cin>>nvalors;
    }
    int N[nvalors];
    int *p=N;
    cout<<"ENTRA ELS VALORS: "<<endl;
    for (int i=0; i<nvalors; i++)
        cin>>N[i];

    quicksort(0,nvalors-1,N);
    cout<<"ORDRE CREIXENT:"<<endl;
    if (nvalors>0)
    {
        cout<<*p;
        for (int i=0; i<nvalors-1; i++)
        {
            p++;
            cout<<", "<<*p;
        }
        cout<<endl;
        cout<<"ORDRE DECREIXENT:"<<endl;
        cout<<*p;
        for (int i=nvalors-1; i>0; i--)
        {
            p--;
            cout<<", "<<*p;
        }
    }
    return 0;

}
