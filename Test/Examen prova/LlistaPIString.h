#ifndef LLISTAPISTRING_H
#define LLISTAPISTRING_H

#include<string>
using namespace std;

class LlistaPIString
// Descripcio: una llista amb punt d'interes d'strings; en una llista buida,
//  el punt d'interes es al final
{
    public:
    // CONSTRUCTORS I DESTRUCTOR
        // Pre:--; Post: llista buida
        LlistaPIString();
        // Pre:--; Post: la llista es copia de  o
        LlistaPIString(const LlistaPIString& o);
        // Pre:--; Post: memoria alliberada
        ~LlistaPIString();


     // CONSULTORS
        // Pre:--; Post: ens diu si no hi ha element destacat
        bool final() const;
        // Pre:--; Post: ens diu si la llista es buida
        bool buida() const;
        // Pre: hi ha element destacat; Post: retorna element destacat
        string actual() const;

      // MODIFICADORS
        // Pre: --; Post: l'element destacat es el primer; si la llista es buida, no n hi ha
        void inici();
        // Pre: hi ha element destacat; Post: ha avan�at element destacat
        void seguent();
        // Pre: --; Post: s inserit abans de l'element destacat (si no n hi ha, inserit el darrer)
        void insereix(string s);
        // Pre: hi ha element destacat
        // Post: el que era element destacat ha estat eliminat; ara el destacat es el seguent
        void elimina();

      // OPERADORS REDEFINITS
        // Pre: --; Post: la llista es copia de  o
        LlistaPIString& operator=(const LlistaPIString& o);


    private:
        struct Node {
           string valor;
           Node* seg;
           Node* ant;
        };

        Node* a_primer;
        Node* a_darrer;
        Node* a_act;   // marca el punt d'interes

        // Pre: llista buida; Post: aquesta llista es copia de  o
        void copia(const LlistaPIString& o);
        // Pre: --; Post: memoria alliberada
        void allibera();

};

#endif // LLISTAPISTRING_H
