// Donada una seq. de strings des de teclat acabada en #, mostrara primer els comencats en digit,
// despres els comencats en majuscula, despres els com. en minuscula i finalment els altres.
// Fara una copia de l'estructura, n'eliminara tots menys els comencats en minuscula i mostrara l'original
// i la copia

#include <iostream>
#include "LlistaPIString.h"

using namespace std;

// Pre: --
// Post: segons si majusc es cert o fals, respectivament,
// l'element destacat de l es la seva primera paraula comencada en minuscula
// o en caracter especial (no lletra ni xifra);
// si no hi ha minúscula, el destacat sera la priemra paraula comencada en c.especial (si n'hi ha);
// si no hi ha ni minuscula ni caracter especial, no hi haura destacat
void situaPrimeraLletra(LlistaPIString & l, bool majusc) {
    l.inici();
    bool trobat= false;
    while (not l.final() and not trobat) {
       char ini= l.actual().at(0);
       if (majusc) trobat= ini>='a' and ini<='z';
       if (not trobat) trobat= not((ini>='a' and ini<='z') or (ini>='A' and ini<='Z') or (ini>='0' and ini<='9')); // c.especial
       if (not trobat) l.seguent();
    }
}

// Pre:--; Post: l'element destacat es el final
void situaAlFinal(LlistaPIString & l) {
    //l.inici();  // prescindible
    while (not l.final())
       l.seguent();
}

int main()
{
    LlistaPIString l; // constructor sense parametres
    string s;
    cout << "INTRODUEIX PARAULES, ACABADES EN #; " << endl;
    cin >> s;
    while (s!="#") {
       char c= s.at(0); // primer caracter
       if (c>='0' and c<='9')   // digit
          l.inici();
       else if (c>='A' and c<='Z')  // majuscula
          situaPrimeraLletra(l,true);
       else if (c>='a' and c<='z')  // minuscula
          situaPrimeraLletra(l,false);
       else // altres
          situaAlFinal(l);
       l.insereix(s); // inserim alla on toca
       cin >> s;
    }

    LlistaPIString minusc= l;  // s'invoca constructor copia
    // a minusc eliminarem totes menys les comencades per minuscula
    minusc.inici();
    while (not minusc.final()) {
       string s= minusc.actual();
       if (s.at(0)<'a' or s.at(0)>'z') minusc.elimina();
       else minusc.seguent();
    }

    cout << "TOTES LES PARAULES: " << endl;
    for (l.inici(); not l.final(); l.seguent()) // recorregut
       cout << l.actual() << endl;
    cout << "PARAULES COMENCADES PER MINUSCULA: " << endl;
    for (minusc.inici(); not minusc.final(); minusc.seguent()) // recorregut
       cout << minusc.actual() << endl;
    return 0;
}
