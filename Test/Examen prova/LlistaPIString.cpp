#include "LlistaPIString.h"

// CONSTRUCTORS I DESTRUCTOR
        // Pre:--; Post: llista buida
        // O(1)
        LlistaPIString::LlistaPIString() {
           a_primer=a_darrer=a_act=NULL;
        }
        // Pre:--; Post: la llista es copia de  o
        // O(n) (n= nombre d'elements d'o)
        LlistaPIString::LlistaPIString(const LlistaPIString& o){
           LlistaPIString();
           copia(o);
        }
        // Pre:--; Post: memoria alliberada
        // O(n)
        LlistaPIString::~LlistaPIString() {
           allibera();
        }

// CONSULTORS
        // Pre:--; Post: ens diu si no hi ha element destacat
        // O(1)
        bool LlistaPIString::final() const {
           return a_act==NULL;
        }
        // Pre:--; Post: ens diu si la llista es buida
        // O(1)
        bool LlistaPIString::buida() const {
           return a_primer==NULL;
        }
        // Pre: hi ha element destacat; Post: retorna element destacat
        // O(1)
        string LlistaPIString::actual() const {
           return a_act->valor;
        }

      // MODIFICADORS
        // Pre: --; Post: l'element destacat es el primer, o es al final si la llista es buida
        // O(1)
        void LlistaPIString::inici() {
           a_act= a_primer;
        }
        // Pre: hi ha element destacat; Post: element destacat es el seguent
        // O(1)
        void LlistaPIString::seguent() {
           a_act= a_act->seg;
        }
        // Pre: --; Post: s  inserit abans de l'element destacat. Si no n'hi ha, inserit com a darrer
        // O(1)
        void LlistaPIString::insereix(string s) {
           Node * nou= new Node;
           nou->valor= s;
           nou->seg= a_act; // just abans de l'element destacat
           if (a_primer==NULL) {  // llista buida
              nou->ant= NULL;
              a_primer=a_darrer= nou;
           }
           else {  // llista no buida
              if (a_act==NULL) { // insercio al final
                 nou->ant= a_darrer;
                 a_darrer->seg= nou;
                 a_darrer= nou;
              }
              else {  // insercio no al final
                 nou->ant= a_act->ant;
                 if (a_act==a_primer) a_primer= nou;// insercio al principi
                 else a_act->ant->seg= nou;  // anterior->nou
                 a_act->ant= nou;            // nou<-a_act
              }
           }
        }
        // Pre: hi ha element destacat
        // Post: el que era element destacat ha estat eliminat, ara el destacat es el seguent
        // O(1)
        void LlistaPIString::elimina() {
           if (a_act==a_darrer) { // eliminem el darrer
              a_darrer= a_darrer->ant;
              if (a_darrer==NULL) a_primer= NULL; // nomes hi havia 1 element
              else a_darrer->seg= NULL;
              delete a_act;
              a_act= NULL;
           }
           else if (a_act==a_primer) { // eliminem el primer
              a_primer= a_primer->seg; // no pot ser NULL !!
              a_primer->ant =NULL;
              delete a_act;
              a_act= a_primer;
           }
           else {  // eliminem un del mig
              a_act->ant->seg= a_act->seg;
              a_act->seg->ant= a_act->ant;
              Node* next= a_act->seg;
              delete a_act;
              a_act= next;
           }
        }

      // OPERADORS REDEFINITS
        // Pre: --; Post: la llista es copia de  o
        // O(n) (n= max(elements de l'objecte, elements de o))
        LlistaPIString& LlistaPIString::operator=(const LlistaPIString& o)
        {
           if (this != &o){
             allibera();
             a_darrer=a_act=NULL;
             copia(o);
           }
           return *this;
        }

// --------------------- METODES PRIVATS

        // Pre: llista buida; Post: aquesta llista es copia de  o
        // O(n) (n= nombre d'elements d'o)
        void LlistaPIString::copia(const LlistaPIString& o) {
           if (not o.buida()) {  // si es buida, no cal fer res
              Node* ant;
              a_primer=a_darrer= new Node;
              a_primer->valor= o.a_primer->valor;
              a_primer->ant= NULL;
              if (o.a_act==o.a_primer) a_act= a_primer;
              for (Node* p= o.a_primer->seg; p!=NULL; p=p->seg) { // recorrem els nodes
                 a_darrer->seg= new Node;
                 ant= a_darrer;
                 a_darrer= a_darrer->seg;
                 a_darrer->valor= p->valor;
                 a_darrer->ant= ant;
                 if (o.a_act==p) a_act= a_darrer;
              }
              a_darrer->seg= NULL;
           }
        }

        // Pre: --; Post: memoria alliberada
        // O(n)
        void LlistaPIString::allibera() {
           while (a_primer!=NULL) {
              Node* aux= a_primer;
              a_primer= a_primer->seg;
              delete aux;
           }
        }

