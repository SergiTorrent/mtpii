#ifndef DATA_H
#define DATA_H


class Data
{
    public:
        Data();
        Data(unsigned d, unsigned m, unsigned a);
        void igualar(Data d);
        bool esMenor(Data d);
        void llegirData();
        void mostrarData() const;

    protected:

    private:
        unsigned any;
        unsigned mes;
        unsigned dia;

        bool esDeTraspas(unsigned any)
    {
        return (!(any%4) && (any%100) || !(any%400));
    }


    bool esValida(unsigned any,unsigned mes,unsigned dia)
    {
        unsigned mesllarg[]= {31,28,31,30,31,30,31,31,30,31,30,31};
        if (!any || !mes || !dia || mes>12)
            return 0;
        if (esDeTraspas(any) && mes==2)
            mesllarg[1]++;
        if (dia>mesllarg[mes-1])
            return 0;
        return 1;
    }
};

#endif // DATA_H
