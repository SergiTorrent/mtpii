#include <iostream>
#include "Empleats.h"

using namespace std;

int main()
{
    unsigned comptador=0;
    cout << "ENTRA ELS EMPLEATS (CODI:0 PER ACABAR)" << endl;
    Empleats jove;
    Empleats actual;
    actual.llegir();
    jove.igualar(actual);
    while (!actual.finalitzacio())
    {
        comptador++;
        actual.llegir();
        jove.MesPetit(actual);
    }
    cout<<"HI HA "<<comptador<<" EMPLEATS"<<endl;
    jove.mostrarEmpleat();
    return 0;
}
