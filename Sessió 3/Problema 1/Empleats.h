#ifndef EMPLEATS_H
#define EMPLEATS_H
#include "Data.h"
#include <iostream>

using namespace std;


class Empleats
{
    public:
        Empleats();
//        Empleats(unsigned c, string n, string c, string p, Data d);
        void MesPetit(Empleats e);
        void llegir();
        bool finalitzacio();
        void igualar(Empleats e);
        void mostrarEmpleat();

    protected:

    private:
            unsigned codi;
            string nom;
            string cognom;
            string poblacio;
            Data data;
};

#endif // EMPLEATS_H
