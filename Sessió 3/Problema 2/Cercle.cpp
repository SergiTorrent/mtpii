#include "Cercle.h"
#include "Punt2D.h"
#include <iostream>

using namespace std;

Cercle::Cercle()
{
    radi=0;
}

void Cercle::llegir()
{
    centre.llegir();
    cin>>radi;
}
void Cercle::llegirCentre()
{
    centre.llegir();
}
void Cercle::llegirRadi()
{
    cin>>radi;
}
void Cercle::mostrar() const
{
    cout<<"Centre: ";
    centre.mostrar();
    cout<<"Radi: "<<radi<<endl;
}
void Cercle::desplacar()
{
    cout<<"Introdueix el desplašament:"<<endl;
    Punt2D aux;
    aux.llegir();
    centre.moure(aux);
}
void Cercle::escalar(double c)
{
    radi*=c;
}
bool Cercle::consultarInterior() const
{
    Punt2D p;
    p.llegir();
    if (centre.distancia(p)>radi){return false;}
    else return true;

}
