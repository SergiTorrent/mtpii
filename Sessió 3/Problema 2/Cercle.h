#ifndef CERCLE_H
#define CERCLE_H
#include "Punt2D.h"


class Cercle
{
    public:
        Cercle();
        void llegir();
        void llegirCentre();
        void llegirRadi();
        void mostrar() const;
        void desplacar();
        void escalar(double r);
        bool consultarInterior() const;



    protected:

    private:
        double radi;
        Punt2D centre;
};

#endif // CERCLE_H
