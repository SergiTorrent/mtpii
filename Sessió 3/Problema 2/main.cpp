#include <iostream>
#include "Punt2D.h"
#include "Cercle.h"


using namespace std;


void menuOpcions()
{
    cout<<"m: mostrar el cercle"<<endl;
    cout<<"d: despla�ar el cercle"<<endl;
    cout<<"e: escalar el cercle respecte el centre"<<endl;
    cout<<"i: consultar si un punt �s interior"<<endl;
    cout<<"s: sortir"<<endl;
    cout<<"Entra una opci�:"<<endl;
}

int main()
{
    cout.setf(ios::fixed);
    cout.precision (2);
    Cercle cercle;
    char opcio;
    cout << "Entra el cercle inicial:"<<endl;
    cout<< "Entra el centre del cercle, x y:" << endl;
    cercle.llegirCentre();
    cout<< "Entra el radi [r>0]:" << endl;
    cercle.llegirRadi();
    menuOpcions();
    cin>>opcio;
    while (opcio != 's')
    {
        if (opcio == 'm')
        {
            cercle.mostrar();
        }
        else if (opcio == 'd')
        {
            cercle.desplacar();
        }
        else if (opcio == 'e')
        {
            cout<<"Introdueix el factor d'escala:"<<endl;
            double escala;
            cin>>escala;
            cercle.escalar(escala);
        }
        else if (opcio == 'i')
        {
            cout<<"Introdueix un punt:"<<endl;
            if (cercle.consultarInterior())
            {
                cout<<"El punt �s interior"<<endl;
            }
            else
                cout << "El punt no �s interior"<<endl;

        }
        menuOpcions();
        cin>> opcio;
    }
    return 0;
}
